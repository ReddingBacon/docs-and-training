# Secure Coding Guideline

## Purpose

This guideline provides a high-level, technology-agnostic approach for secure coding and should be used to help create software security requirements within your team.  These conventions are based on industry-standard best practices, including the [OWASP Secure Coding Practices Quick Reference Guide](https://www.owasp.org/index.php/OWASP_Secure_Coding_Practices_-_Quick_Reference_Guide).

## Guideline

This section lists the coding guidelines that should be applied to the development and should be adjusted per the organization
s requirements 


1. **Data Input Validation** - Ensure applications validate input properly and restrictively, allowing only those types of input that are known to be correct. Examples include, but are not limited to, such possibilities as cross-site scripting, buffer overflow errors, and injection flaws.   White list input wherever possible (prefered over blacklisting which can be circumvented) and canonicalize all input beforehand.   Use a centralized input validation routine per application.  Validate all input including (but not limited to):
        1. User input, File uploads, Email addresses, Regular expressions, XML, and code
        2. Validate data types, data range, and data length
        3. See https://www.owasp.org/index.php/Input_Validation_Cheat_Sheet#File_Uploads for more information
2. **Output Sanitization & Encoding** – Ensure application output is as intended for the recipient and validated against attacks, such as Cross-site Scripting, and also that classified data or PI (i.e., social security numbers) are not exposed.  Ensure that detailed system information or unintended data are not exposed.  Also, encode all unsafe characters
        1. https://www.owasp.org/images/0/08/OWASP_SCP_Quick_Reference_Guide_v2.pdf
3. **Access Control / Authentication & Authorization** - Ensure applications processing data properly authenticate users.  Also ensure that the users have proper authorization for the types of data and the types of actions they request. Use centralized authentication and authorization code where possible. 
        1. See more authentication: https://www.owasp.org/index.php/Authentication_Cheat_Sheet#Utilize_Multi-Factor_Authentication 
        2. See more access control: https://www.owasp.org/index.php/Access_Control_Cheat_Sheet 
4. **Session Management** – Ensure application user sessions are not susceptible to session hijacking or other attacks.  
        1. Use built in frameworks where possible
        2. Store sessions on the server (i.e., not on the client or with URL encoding)
        3. Use short timeout intervals
        4. Session IDs should be long and random
        5. Invalidate sessions on the server
        6. Regenerate sessions and session IDs when privileges change
        7. Do not re-use session IDs for any other purpose
        8. See https://www.owasp.org/index.php/Session_Management_Cheat_Sheet
5. ****Error Handling**** - Ensure applications execute proper error handling so that errors will not provide detailed system information, deny service, impair security mechanisms, or crash the system. 
        1. See https://www.owasp.org/index.php/Error_handling for more information and examples.
6. **Logging & Auditing** - Implement the use of application logs to the extent practical.  Logging should be consistent within and across applications, use industry standards, and assist with auditing, monitoring and identifying security incidents.
        1. See more https://www.owasp.org/index.php/Logging_Cheat_Sheet 
7. **Secure Data in Storage** **and During Transmission** – Ensure that sensitive data is stored and transported using proper encryption.   
        1. DO NOT ROLL YOUR OWN CRYPTO.  This includes adding your own entroy to key generation or an additional protocol to encrypting comms.
        2. All cryptographic protocols, algorithms, and tools should be vetted
        3. Ensure certificates are properly validated and avoid wildcard certificates
        4. Where possible develop wrapper libraries
        5. Maintain a list of approved cryptographic technologies
        6. Key Generation, storage, and maintenanace are just as important. Use an approved key gen function and key store.  Keys must be shared securily and managed with a secure lifecycle process.  Please carefully read the following OWASP Cheat Sheets. 
        7. Data at rest: https://www.owasp.org/index.php/Cryptographic_Storage_Cheat_Sheet  
        8. Transit: https://www.owasp.org/index.php/Transport_Layer_Protection_Cheat_Sheet 
8. **Static Code Analysis** – Use a Static Code Analysis tool to evaluate all code during implementation of software.  Identify and fix all positives and mark false positives. 
    1. **Mandatory Code Review** - Conduct code-level security reviews with professionally trained peers for all new or modified applications or software components.  Peer review must include feedback and be properly documented. 
9. **Conduct Periodic Security Tests** – A member(s) of the Software Security Group will conduct internal security tests of Internet applications for every major release and 3rd party penetration tests for Internet applications annually.
10. **Secure Deployment** – Ensure that development or testing artifacts including scripts, configuration files, test data, test code, unit test, etc.… are not deployed to production. 
11. **General Coding Practices** - Developers should be trained and understand the OWASP (Open Web Application Security Project) Top 10 vulnerabilities and how to mitigate them.  While OWASP specifically references web application, the secure coding principles can be applied in non-web applications as well.  The secure coding techniques illustrated in the Top 10 should be applied at design-time, during implementation, when using Static Code Analysis tools, code review, and when testing.  
        1.  [OWASP Top 10](https://www.owasp.org/index.php/Top_10_2013-Top_10)
12. ****<WIP> — **Secure & Hardened Developer Workstations**
        1. Only install necessary software.  Remove everything else.
        2. Patch all OS and software regularly
        3. Account authorization should never be admin.  Elevate privileges only when needed.
        4. Access to system limited to developer only.  System can only attach to verified network.
        5. Implement software change control.  
        6. Segregation of Duties, environments.  Isolation of development/production environments.
        7. Software-Supply Chain management.

## References

The following material can be used to extend or aid with bettering the secure coding guidelines:

    * _Secure Coding Practices Quick Reference Guide _
    * [_OWASP Code Review Guide_](https://www.owasp.org/images/2/2e/OWASP_Code_Review_Guide-V1_1.pdf)
    * _Department of Homeland Security _[_secure coding practices_](https://buildsecurityin.us-cert.gov/resources/secure-coding-sites)

 
 
 

