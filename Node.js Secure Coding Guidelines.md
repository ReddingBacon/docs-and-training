# Node.js Secure Coding Guidelines


This document is intended as a starting point for discussing secure Node.js coding standards.

Begin with [Secure Coding Guideline](https://quip.com/IhxMAajkjwFr) and then add the following:

Checklists:

* https://expressjs.com/en/advanced/best-practice-security.html
* https://blog.risingstack.com/node-js-security-checklist/

## Inject Attacks

Review input validation from [Secure Coding Guideline](https://gitlab.com/ReddingBacon/docs-and-training/-/blob/master/Secure%20Coding%20Guideline.md) as this is key to stopping many attacks.  Also review the OWASP [Injection Attacks](https://www.owasp.org/index.php/Top_10_2013-A1-Injection) page.

### Command Injection

The `child_process.exe()` function call (a core module function) enables you to invoke OS commands.  Do not use it.  If it's necessary to make OS commands then you can lessen the risk of injection using the following:


* Input validation - Validate all user input.  Whitelist input if possible.
* User execFile() or spawn() instead of exec() - these separate the command from the parameters, making it difficult to chain commands.  These can not completely reduce injection risk.

### Server Side JS Injection

When `eval()`, `setTimeout()`, `setInterval()`, `Function()` are used to process user provided inputs, it can be exploited by an attacker to inject and execute malicious JavaScript code on the server.  Unfiltered input from attackers could be something like:

* DOS:  `while(1)` or `process.exit()`
* File system access: `res.end(require('fs').readFileSync(filename))`

To prevent server-side js injection attacks:

* Validate user inputs on server side before processing
* Do not use `eval()`function to parse user inputs. Avoid using other commands with similar effect, such as `setTimeOut()`, `setInterval()`, and `Function()`.
* For parsing JSON input, instead of using `eval()`, use a safer alternative such as `JSON.parse()`. For type conversions use type related `parseXXX()`methods.
* Include `"use strict"`at the beginning of a function, which enables [strict mode](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions_and_function_scope/Strict_mode)within the enclosing function scope.
* User parseInt() or similar where possible:  `eval(req.body.time_interval);` is dangerous so use `parseInt(req.body.time_interval);`

### SQL Injection

* Never allow input to flow directly to database without filtering and validation
* Use parameterized queries (or parameterized queries) instead of creating SQL directly.
* Input validation - All input must be validated.  Whitelist valid input if possible.
* Use well-known anti-sql injection libraries to filter, validate, and/or encode input.

### No-SQL Injection

* Validate user input
* Filter, escape, and/or encode input using well support and well-known libraries.

### Prototype Pollution

Image result for prototype pollution owasp
Prototype Pollution is a vulnerability that allows attackers to exploit the rules of the JavaScript programming language, by injecting properties into existing JavaScript language construct prototypes, such as Objects to compromise applications in various ways.
* [More on PP](https://portswigger.net/daily-swig/prototype-pollution-the-dangerous-and-underrated-vulnerability-impacting-javascript-applications)
* [Another Example](https://www.whitesourcesoftware.com/resources/blog/prototype-pollution-vulnerabilities/)

This is another form of injection.  Never trust data from beyond the scope of the application and follow best practices such as input validation.  
* Object.freeze will mitigate almost all cases. Freezing an Object prevents new Prototypes from being added to it.
* Using schema validation to ensure that the JSON data contains the expected attributes. This will remove __proto__ if it appears in the JSON. 
* Using map primitive, which was introduced in the EcmaScript 6 standard, and is now well-supported in the NodeJS environment.
* Objects created using the Object.create(null) function won’t have the__proto__ attribute. 
* In general, pay attention when using recursive merge functions, since they are more prone to Prototype Pollution vulnerabilities than other functions.  

## XSS Attacks

Review [Cross Site Scripting Attacks](https://www.owasp.org/index.php/Cross-site_Scripting_(XSS)) (XSS).

The XSS flaw occurs when the application code takes untrusted data supplied by a user and renders it in the browser without proper validation or escaping. In this case, the browser is unable to distinguish between the script added by the application developer and the piece of malicious script injected by an attacker.


* Use CSP (content-security-policy) response headers.  Setting `default-src` to `self` allows the browser to download JavaScript code only from the same domain from which the page was loaded.  Example CSP response header  using the *helmet* module to restrict JS only from the server (*and no inline JS? verify*) but allows images from a different but specific domain:

```

`var policy = { 
    defaultPolicy: { 
        "default-src": ["'self'"],  // Default setting allows JS from server only 
        "img-src": ["static.example.com"] // Allow images from static.example.com
    }
}
helmet.csp.policy(policy);`
```

* Input validation.  Whitelisting where possible.



* Encode untrusted, user-supplied data.  Use a well-known and trusted module, i.e., DOMPurify.

```
Use the `httpOnly` cookie flag to help prevent stealing sessions, one of the most goals of XSS.    Example:
`var session = require("express-session");
app.use(session({ secret: "s3Cur3", 
    key: "sessionId", 
    cookie: { 
        httpOnly: true, <a class="co" id="icon_cross_site_scripting__xss__CO2-1" href="#item_cross_site_scripting__xss__CO2-1"><img src="callouts/1.png" alt="1"/></a> 
        secure: true 
    }
}));`
```

* 

## Indirect Object Reference

A direct object reference occurs when a developer exposes a reference to an internal implementation object, such as a file, directory, database record, or key, as a URL or form parameter. An attacker can manipulate direct object references to access other objects without authorization, unless an access control check is in place.

This example shows a app that pulls PDF help files off the file system, but an attacker can change which file it's accessing by modifying the URL

```
www.example.com/help?name=browsetoolhelp.pdf
www.example.com/userProfile?id=12345
```

**Directory Traversal**  is a special case of the insecure direct object references vulnerability, in which an attacker manipulates the path exposed in a URL to access directories and files outside of the web root folder and possibly compromise the entire web server.
To exploit it, an attacker uses absolute or relative path traversal characters such as `/`, `../`, or its encoded versions `%2f`, `%2e%2e%2f`, or `%2e%2e/` to manipulate the path.  See [OWASP Directory Traversal](https://www.owasp.org/index.php/Path_Traversal) for more information.

Mitigations:

* Do not pass information from client to server.  Instead use information already in the user's session.  Example, if users need to access the profile information do not pass the profile ID from the client, get it from the session
* Use an *indirect reference map* technique. The idea behind this is to substitute the sensitive direct internal reference in URL parameters or form fields with a random value that is difficult to predict (such as a GUID) or specific only to the logged-in user (such as sequential values per user or session).  See [OWASP Indirect Object Reference](http://bit.ly/2o1fLbs).
* Validate, on the server, every request that the user is authenticated and authorized for that resource (functionality, object or data). Never rely on client side validation as it is easily compromised.
* For directory traversal:
    * At the system-configuration level, configure the root directory and access control list to confine the application user’s access and restrict access to files and directories outside of the web root.
    * In the application code, validate user inputs for presence of path traversal and corresponding encoded characters.

## Secure Supply Chain

Ensure dependencies are secure using *nsp* and *Snyk...*

## More...

[TODO]

## Storing Sensitive Data

This section might be another topic/page but should work as a good starting point for defining what is sensitive data and how to secure it at rest and during transit.

### Sensitive Data

*Sensitive* data can include:

* Passwords
* Passphrases
* Encryption keys
* OAuth tokens
* Purchase instruments, such as credit card numbers
* Personal contact information such as names, phone numbers, email addresses, account usernames, physical addresses, and more
* Demographic information such as income, gender, age, ethnicity, education
* In some states and countries: machine identifying information such as MAC address, serial numbers, IP addresses, and more

Sensitive data is also called *personally-identifying information* (PII) or *high business impact* (HBI) data. What is considered sensitive data varies greatly from state to state and country to country. Various compliance standards, such as the Payment Card Industry (PCI) compliance standard, require special steps to be taken when collecting sensitive data in order to stay in compliance.

### Do Not Hard code  Secrets

Storing sensitive information in the source code of your application might not always be a good practice, anyone that has access to the source code can view the secrets in clear text.

### Debug logs

Debug logs in apex code should not contain any sensitive data (usernames, passwords, names, contact information, opportunity information, PII, etc). The debug logs include standard salesforce logs using system.debug() methods or custom debug logs created by the application. Sensitive information should also be not be sent to 3rd party by emails or other means as part of reporting possible errors.

### Sensitive Info in URL

Long term secrets like username/passwords, API tokens and long lasting access tokens should not be sent via GET parameters in the query string. It is fine to send short lived tokens like CSRF tokens in the URL. Salesforce session id or any PII data should not be sent over URL to external applications.

### Integration With External Applications

Do not store username/passwords or other sensitive information of other application that are integrated.  Instead use OAuth or other federated frameworks and protocols.

### When storing sensitive information on a machine:

* All authentication secrets must be encrypted when stored on disk. This includes passwords, API Tokens, and OAuth Tokens.
* For client apps running on a desktop, laptop, tablet, or mobile device, store all secrets in the vendor provided key store (keychain in OS X/iOS devices, keystore in Android devices, or in the registry protected with the DP-API on windows devices.) This is a hard requirement to pass the security review.
* For services running on servers that must boot without user interaction, store secrets in a database encrypted with a key not available to the database process. The application layer should provide the key as needed to the database at runtime or should decrypt/encrypt as needed in its own process space.
* Do not store any cryptographic keys used for protecting secrets in your application code
* Be cautious of the algorithms and ciphers used in any cryptographic operations
* Salt hashes, and if possible store salts and hashes separately
* Leverage strong platform cryptographic solutions
* Check if frameworks/platforms have already addressed the problem
* Use SSL/TLS to transmit sensitive data

## Other Attacks Common to Node.js

Following are some notes on other common attacks worth exploring for Node:

* Encoding issues:
    * URL encoding directory traversal
    * XSS - encoding JS into input fields using HTML encoding or base64
    * template engine escaping (ie, Mustache template module) :  
        * template:  `<img src={{user_input_val}}>`
        * user input value:  [`http://evil.com/avatar.jpg`](http://evil.com/avatar.jpg)` onload=alert(1);`
        * This happens because “=” and whitespace weren't included on the list dangerous characters that should have been escaped or filtered (still have to worry about encoding).
        * Don't allow tripple-bracketing `{{{` because this tells the interpreter to NOT encode/filter anything
    * Use frameworks, but get more specific with their configuration for the app you're working with, apply more filtering.  Critique their default configurations.
* Type Manipulation
    * qs module - de facto standard that parses URL query strings.
        * `qs.parse(“a=foo&b=bar”)` generates JSON object `{ a: “foo”, b: "bar" }`
        * Attachers can pass <script>alert(1);</script>
        * A solution is to escape all chars before consumption `lib.escape(user_input.toString());`



