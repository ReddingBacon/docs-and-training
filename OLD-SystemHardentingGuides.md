# System Hardening Guidelines
# OLD - This needs updating

This document is intended to provide some guidance when securing devices, whether corporate or personal. These guidelines are not enforceable rules. They are tools to assist with streamlining the process of securing your information technology. Many of the recommendations outlined here are general in nature and may not take into account a device’s intended purpose (i.e., server vs. workstation). Alteryx policies may incorporate, override, or ignore this guideline.

## Windows

* Only keep software you need.  Remove all software that you don't and only use reputable apps that you need for work.  Disable unnecessary services (including IPv6)
* Patch the system regularly
    * Browsers
    * Java (remove this if possible)
    * All Adobe products (Flash, Reader, etc.)
* Do not log in as Administrator.  Use a normal account.  Only run applications as Admin when necessary.
* Use corporate security policy (GPO, AD authentication)
* Use endpoint protection or enable host firewall
* Strong Passwords - long, upper and lowercase, numbers, and special characters.  Change frequently (at least every 3 months).  Temporarily lock after multiple failed attempts
* Use a VPN when working remote or when using wifi.
* Physical security. (Provide lock & key at user request.)
* Encrypt the hard drive
    * Use LANDESK for key management
* Use only for work purposes.  For personal use log out and log back in with another profile
* Lock down USB drive permissions with LANDESK
* Jungle disk for backups
* Don't user wireless/bluet keyboards/devices 

## Linux

* Only keep software you need.  Remove all software that you don't and only use reputable apps that you need for work.  Disable unnecessary services (including IPv6)
* Patch the system regularly (putting it in a `cron` job is a good idea)
* Users should not be given root privileges.  sudo rights when necessary.    
* On servers
    * Disable remote sessions for root.  Users must remote in and escalate privileges
    * Disable password logins unless bound to domain
* Strong Passwords - long, upper and lowercase, numbers, and special characters.  Change frequently (at least every 3 months).  Temporarily lock after multiple failed attempts
* Secure remote communication protocols only, such as SSH, SFTP, FTPS, etc...
* Wherever possible use corporate authentication and not local.  
* Use SELinux if possible
* Install HIDS software
* Configure IPTables or firewall. Justify any listeners. Install fail2ban, sshguard, or another adaptive realtime blackhole for any listening services.
* Encrypt the hard drive
* Linux backups TBD

## secure browser settings 

* Ad Blocker
* Disable JavaScript
* Disable pop-ups
* Disable location tracking
* Enable Malware Protection (Chrome)
* Cookies: Select “Keep local data only until I quit my browser” and “Block third-party cookies and site data.” 
* Disable auto-fill of passwords | Disable saving passwords
* Built-in support for client-side XSS filtering???  (Chrome does, firefox?)

## wifi

* Change the SSID to something unique so devices don't try to auto-connect to similar nets (disable auto connect on devices as well)
* patch WAP devices , routers regularly
* Strong passwords, change regularly
* Enforce WPA2 Encryption
* Limit connections
* Create separate business network.  Put non-employees on separate network  
* Use a WAP that has strong security features 



## Mobile Devices

### iPhone

* Remove all unnecessary apps, don't install new ones you don't need.
* Use 6 digit or alphanumeric codes to lock the screen: Settings>Touch ID & Passcode, select Change Passcode and then tap the small Passcode Options
* Configure a short auto-lock: Settings>General>Auto-Lock
* Disable Control Center on locked screen:  Settings > select Control Center > Access on lock screen
* Disable Siri on locked screen:  Settings > Touch ID & Passcode.  Also, turning off geocoded images can be a good idea:  Settings > Privacy > Location Services > Camera and then select Never.
* Turn on Find My iPhone:  Settings > iCloud
* Configure the phone to erase all data after 10 failed login attempts:  Settings>Touch ID & Passcode
* Turn off location information gathering:  Settings>Privacy>Location Services>System Services and then Frequent Locations
* Disable location services for apps that don't need it:  Settings>Privacy>Location Service
* Safari:
    * Limit ad tracking:  Settings>Privacy>Advertising 
    * Install an ad blocker such as BlockIt
    * Change your search engine to DuckDuckGo in Settings>Safari>Search Engine,because the search engine doesn’t collect information about you.
* Wifi
    * Ask to join networks:  Settings > wifi
    * Use a VPN over wifi networks
    * Use a security app such as Skycure
* Enable two-factor authentication for iCloud http://www.gottabemobile.com/2014/09/23/how-to-enable-two-factor-authentication-in-icloud/
* Use a password manager.

## Macbook

...

* * *
[More ideas](http://www.cyberciti.biz/tips/linux-security.html)
More [tips](https://www.veracode.com/blog/2013/03/browser-security-settings-for-chrome-firefox-and-internet-explorer)
