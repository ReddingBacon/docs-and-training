# Credit & Identity Protection Checklist
## Freeze Your Credit
* Freeze your credit for all 3 credit bureaus.  [More info](https://www.consumer.ftc.gov/articles/what-know-about-credit-freezes-and-fraud-alerts#freeze) about credit freezes and fraud alerts.
* Equifax [https://www.equifax.com/personal/credit-report-services/](https://www.equifax.com/personal/credit-report-services/)
* [Experian.com/help](https://www.experian.com/help/) 888-EXPERIAN (888-397-3742)
* [TransUnion](TransUnion.com/credit-help)  888-909-8872

## Review Your Credit
Review your credit from all 3 bureaus and check for fraudulent activity or accounts.  Schedule/automate regular reports so you can review it regularly (6 months or yearly)

## Lock Your Social Security
You can get some level of ID fraud proctection by locking your social security number https://www.e-verify.gov/mye-verify/self-lock

## Confifgure Multi-Factor Authentication
This is a critical step.  Use Multi-factor Authentication (MFA) on all web sites that offer it, but begin with the most important. 
1. Creat a list of the most important websites and tools such as banks, investment sites, and email.  You will need this list for MFA and later for securing your passwords.
1. For each go to the website setting for your account and configure MFA.  There are several different types including adding Google Authenticator or similar authenticator app (best type of MFA option) or text/call your phone (2nd best option).
1.  Recommend this for all you accounts.  After you've completed the list you can add MFA to other sites as you access them in the future.


## Use a Passowrd Manager
Research and use a password manager such as LastPass or 1Password. _You must spend time learning this tool and it must become a way of life_.  

1. [Understand why you need this](https://www.howtogeek.com/141500/why-you-should-use-a-password-manager-and-how-to-get-started/).
1. Spend time learning the tool.  If you get in a hurry you screw this up and lose access to all the sites you're managing.  
1. 1. Using the list you created in the previous step, go to each site and chage the password using the the password manager's password generator to create complex passwords.  I use as many characters as the site will allow, usually around 32 chars including upper and lower case, numbers, and special chars.  They look like this: `8zD&ON@zX9uwPppfIz1aI&qXopVW9ZiQ`
1.  Recovery Codes:  Very important if you lose your phone or access to the password manager this will save your ass.  Generate and save recovery codes for each site.  Password managers often have a Notes field for each site.  Save recovery codes here.  Generate recovery codes for the password manager as well and save them is a secure place that is not on your phone.  

## Identity Theft Insurance
These services are pretty much garbage... but you still need it :-(.  Check with your bank or similar sevices to see what they offer.  You can also consider other services like LifeLock, Idnetity Guard, etc...

## Secure Your Devices and Home Network
This is a separate checklist.  Here are [a few things you can do.](https://gitlab.com/ReddingBacon/docs-and-training/-/blob/master/OLD-SystemHardentingGuides.md).  

## Do This For The Family
You need to do the previous actions for everyone in your immediate family.

## If You Susupect ID Theft
Review this:  https://www.justice.gov/criminal-fraud/identity-theft/identity-theft-and-identity-fraud
- Call the companies were the activity may have occured. 
- Create Fraud Alerts:  To place a fraud alert, contact one of the three credit bureaus. That company must tell the other two.
-- Experian.com/fraudalert
1-888-397-3742
--TransUnion.com/fraud
1-800-680-7289
--Equifax.com/CreditReportAssistance
1-888-766-0008
- Report to the FTC: Go to IdentityTheft.gov or call 1-877-438-4338. Include as many details as possible.
Based on the information you enter, IdentityTheft.gov will create your Identity Theft Report and personal recovery plan.
- Contact your ID Theft Service


