# FISMA/NIST Security Compliance

## Introduction

To sell secure applications in the open market, all software vendors eventually realize that they must be comply with security standards.  These security standards require that the vendor implement secure management processes, technologies and development techniques.

This begs the question — which compliance standard will allow them to fully participate in a number of overlapping markets such as financial, banking, defense, healthcare, energy, utilities etc?  An organization usually wont have the financial resources nor the organizational impetus to target multiple standards.  The good news is that most of the standards specify relatively similar requirements.  If you're “compliant” with the requirements in one standard, you're likely to be nearly compliant to others -- especially if the chosen standard is more generally useful across markets.  As an example, security requirements for HIPAA healthcare standards are a small subset of other more general standards.  The only differences are generally those that affect the sharing of private healthcare data. 

This document explores using FISMA/NIST as one obvious choice.   FISMA/NIST outlines the security requirements of information systems in use within US Federal agencies and subcontractors.  So many organizations interact with the US government that it's the closest thing to a standard that we have.   In 2002, the US Congress passed FISMA  (Federal Information Security Management Act) as a set of privacy and security regulations.  These regulations were intended to define a comprehensive framework to protect federal information systems, operations and assets against natural or man-made threats.

FISMA legislation specifies compliance with the standards defined by NIST (National Institute of Standards and Technology) — specifically NIST 800-53.  NIST 800-53 contains security requirements for certain environments and situations called “controls”.  These controls are organized into related control groups.  Control groups can be generally categorized as “technical controls”, “operational controls” and “engineering best practices” controls.

Using FISMA/NIST 800-53 as a standard provides a general approach to application security that is comprehensive, focused and measured.  It is also economical by being widely useful across markets. 

## Compliance Challenges

Though 800-53 specifies a large and varied number of controls, it can be challenging to apply it to specific situations since it stops just short of recommending particular solutions. 

Another common misconception about 800-53 is that it confers compliance on a particular piece of software. It cannot.  It can only specify requirements regarding a particular information system deployed and running  in a ***FISMA compliant data center*** (which has its own own controls).  In this way FISMA compliance can be viewed as a a set of technical and operational controls on the entire information system “stack” — from the physical data center all the way to the highest layers of an application deployed in that data center.

From these specifications security assessors then analyze the candidate information system to be deployed and report on any deficiencies that require mitigation. The organization then schedules remediation plans and eventually a senior official within each organization signs an “Authority To Operate” after which the application can be moved into production.

This process is time-consuming and expensive.  It also raises other questions, such as: 

1. If we deploy the same information system multiple times in the same organization, will it need separate ATOs for each?
2. If the same information system is deployed in multiple US government agencies, will it need to be re-analyzed from scratch for every agency?
3. Is there anything a software vendor can do to minimize the FISMA compliance overhead (and related long development and sales cycle) when selling the same information system into multiple federal agencies?  In other words, what is the closest that an application can approach to FISMA/NIST compliance without actually  being deployed?
4. How does FISMA/NIST specify operational and development requirements for third-party software and service vendors?

## FedRamp Certification

Several years ago these questions were very much on the minds of software vendors because the time and labor expended on federal security compliance was becoming cost prohibitive.   In order to survive software vendors were forced to build these costs into their consulting and deployment contracts. These costs were then passed onto the tax payer. 

Eventually the Federal government developed the “FedRamp” compliance program (which includes a special set of NIST 800-53 controls) to qualify cloud applications and services so that they could be purchased from on a security-compliant GSA contract.  This allowed the agencies to deploy these applications with a minimum amount of security acceptance paperwork (essentially just an ATO signature from the agency).

Why cloud applications and not all applications?

The FISMA/NIST process can only certify an information system as deployed in a specific target environment.   In order to evaluate the application, the most practical, general and economical target environment is a secure cloud environment.  In this case the application or information system is assumed to be deployed into a cloud data center which itself must be FISMA compliant.

What about applications that aren't cloud based?   The same control sets can be applied to them as well.  Any application built to FISMA compliance appropriate for a particular use will have nearly the same requirements.  If it's not a cloud app then it will be checked against FISMA requirements and if these are met,  then it's deployment will be into the data center of the Federal agency in question.  Those data centers being largely FISMA compliant, the roll-out will be remarkably similar.  In many respects FedRamp requirements are more stringent than applications being deployed within the Federal government data center.   If a FedRamp compliant application is to be deployed internally, many of these controls are relaxed or already assumed to be compliant. 

In this sense there are definite advantages to targeting application architectures be *capable* of cloud deployment. If the application is FedRamp compliant, it can be deployed into either a FedRamp compliant cloud data center or a FISMA compliant Federal agency data center.  Minor differences when deployed into a local Federal FISMA data center can be handled as one-off exceptions. 

### Control Groups

While there are a large number of 800-53 controls that apply to other issues, FedRamp is mostly concerned with the technical , operational and best practices controls involved in the development and operation of the target application or system.

By carefully integrating these controls into the software development life cycle we can effectively move our current software systems into compliance.  By referring to these controls in design and architecture meetings we can economically build security compliance and capability into future products.  

The relevant high-level control groups in NIST 800-53 are as follows:

* Access Control (AC)
* Security Awareness and Training (AT)
* Audit (AU)
* Certification and Assessment (CA)
* Configuration Management (CM)
* Contingency Planning (CP)
* Identification and Authentication (IA)
* Incident Response (IR)
* Maintenance (MA)
* Media Protection (MP)
* Physical and Environmental (PE)
* Planning and Procedures (PL)
* Personnel Security (PS)
* Risk Assessment (RA)
* System and Services Acquisition (SA)
* System and Communications Protection (SC)
* System and Information Integrity (SI)

## Technical Security Controls

The majority of the controls directly applicable to software are the so-called NIST 800-53 “technical” controls. These are either controls which either directly specify engineering requirements or are indirectly required to use the information system in a manner compliant with operational controls.

## Operational Security Controls

A significant number of the NIST controls are operational requirements.  These specify the administrative and management procedures required of any vendor selling FISMA/FedRamp compliant services, from data centers, storage, software and cloud services of any kind.  They are referenced here because:

* Compliant information systems will require technical functions compliant with operational requirements
* Companies and any cloud subcontractors will need to be compliant with controls if we wish sell our services to the Federal government or their subcontractors.

## Security Engineering Principles

Both the original FISMA and later FedRamp standards emphasize the importance of secure development best practices for the design and development of new information systems as well as system upgrades and modifications.  Training of developers and architects in secure engineering best practices is also required.

This requirement is specified in control SA-8.  A complete guide to these principles and practices are contained in NIST 800-27. 


##  Agile Development of Security Requirements

Since security is a requirement of all products compliant with FISMA/NIST and FedRamp, it's imperative to implement a clear model to track these requirements.  

Since all products need to be compliant with the same base requirements, an effective method of recording the base requirements is paramount.  Once they've been created, they can be saved and referenced both in user stories and defects.

We will be exploring the best way to implement this security initiative with both senior management, development and various scrum masters.  The following rough model is a path forward that combines scalability, testability and visibility of security issues. 

## Product Security  Requirements

### Functional Requirements

These are the technical requirements that implement security capabilities and are tracked in user stories against iterations and releases.  These capabilities can be tracked normally, defects filed against them and verified in unit and integration tests.

### Non-Functional Requirements (NFRs, Constraints)

The majority of product security requirements fall within non-functional constraints.  They are cross-cutting across teams, projects etc.  These are typically considered as Definition of Done items, but often drive specific functional requirements and capabilities.  Non functional requirements fall into two broad categories:

1. Internal: These are usually prescriptive best practices or architectural guidelines.  They are verified using manual review, collaboration or auditing
2. External:  These can be verified using behavioral checks.  These checks are typically external scanning, manual or automated testing.

## Non-Product Security Requirements

### Operational / Supporting Stack Requirements

These requirements are internal technical staff processes or systems and networking controls.

### Administration, Planning and HR Requirements

These requirements generally apply to policy and procedure documentation as well as HR and staff screen.

### Best Practice and Training Requirements

These requirements describe secure development best practices, developer and staff security training.





