# Team Charter Template & Discussion Questions

## Vision

Team Vision Statement

## Mission

Team Mission Statement

## Objectives

* **Team Objectives**
    * Details

## Organization

Any Statement/Notes about the Team worth sharing...

        * Team Member
            * 5 Skills worth pointing out
        * 

## Dependent Groups

* What groups does this need to work well with?

* 

## Risks

Following are the greatest risks to achieving the Team mission:

* List Risks
* 



* * *

# Discussion Notes


Why are we doing this?

1. 



* **Team Purpose:** 
    * What kind of team is this exactly (a work team, project team, management team, coordination team)? 
        * 
    * Why does the team exist? 
        * 
    * What “work” does the team do? 
        * 
    * What topics belong “in” this team and what’s “out?” 
        * IN
            * 
        * OUT
            * 
    * What is the team responsible for accomplishing?
        * 
* **Context:** 
    * Who is the team accountable to? 
        * 
    * With what other groups/teams do we connect? What do they want/need from us?
        * 
* **Goals:** 
    * What specific results do we expect from our efforts? 
        * 
    * What outcomes (cost, quality, speed, service, quantity, coordination of X, innovation of X)? 
        * 
    * How can we measure that?
        * 
* **Roles:** 
    * Who is on the team? What perspective does each member bring?  Are there special roles (e.g., leader, facilitator, etc?) or sub-groups within the team? 
        * 
    * Team as whole provides...
        * 
    * Individual skills... (add 5 bullets max)
        * Team Member
            * Skills
    * What do subgroups require of us?
        * 
* **Work Processes:** 
    * What processes will we use to do the team’s work? (List them out, step by step.) 
        * 
    * How often will we meet? 
        * 
    * Who determines and manages our agenda? 
        * 
    * How will we connect with our stakeholders and other sponsors of our work?
        * 
* **Decision-making:** 
    * What decisions are made within this team? 
        * 
    * What is out of bounds? 
        * 
    * What level of decision-making responsibility do we have? 
        * 
    * What decision process will we use?
        * 
* **Communication:** 
    * How will we communicate with each other, and connect to others within the organization? 
        * 
    * For some tools your team can try out to improve communication, see [3 Tools for Teams](https://www.ccl.org/articles/leading-effectively-articles/3-tools-for-teams/).
* **Norms:** 
    * What do we expect of each other? 
        * 
    * How do we agree to handle conflict? 
        * 
    * What are our team norms and/or operating principles? 
        * 
    * For help, see [10 Steps for Establishing Team Norms](https://www.ccl.org/articles/leading-effectively-articles/the-real-world-guide-to-team-norms/).






